const express = require("express"),
	app = express(),
	log = require("winston"),
	PORT = process.env.PORT || 3000,
	controller = require("./metadata/rest"),
	bodyparser = require("body-parser"),
	Service = require("./metadata/service");
	
app.use(bodyparser.json());

app.get("*", (req, res, next)=>{
	res.append("x-feature", "extendedMetadata");
	log.info(`method=${req.method} url=${req.url}`);
	next();
});

const service = new Service();
controller.init({routerRef: app, base: "metadata", service: service});

app.listen(PORT, ()=>{
	log.info(`Application started in port ${PORT}`);
});