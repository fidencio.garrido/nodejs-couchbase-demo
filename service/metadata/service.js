const Db = require("./db"),
	nats = require("./nats");

class Service {
	
	initRoutes(){
		
	}
	
	findAll(stream){
		if (typeof stream==="undefined"){
			const self = this;
			return new Promise((resolve, reject)=>{
				self.db.findAll().then((res)=>{
					console.log("success");
					resolve(res);
				}).catch((err)=>{
					console.log("error?");
					reject(err);
				});
			});
		} else{
			console.log("passing response object");
			this.db.findAllStream(stream);
		}
	}
	
	create(item){
		const self = this;
		return new Promise((resolve, reject)=>{
			item.id = self.itemStore.length;
			self.db.create(item).then((saved)=>{
				resolve(saved);
			}).catch((err)=>{
				reject(err);
			});
			// notify
		});
	}
	
	constructor(options){
		this.itemStore = [];
		this.db = new Db();
	}
}

module.exports = Service;