const log = require("winston");

const defaultOptions = {
	base: "service",
	x: "y"
};

const Rest = {
	options: {},
	service: null,
	initRouters(app){
		log.info(`Initializing routes for ${Rest.options.base}`);
		
		app.get(`/${Rest.options.base}`, (req, res)=>{
			const stream = (req.query.stream) ? true : false;
			if (!stream){
				Rest.service.findAll().then((items)=>{
					res.send(items);
				}).catch((err)=>{
					res.status(500).send(err);
				});
			} else{
				Rest.service.findAll(res);
			}
		});
		
		app.get(`/${Rest.options.base}/:id`, (req, res)=>{
			res.send("ok");
		});
		
		app.post(`/${Rest.options.base}`, (req, res)=>{
			const item = req.body;
			log.info("Adding item");
			Rest.service.create(item).then( (saved)=>{
				res.send(saved);
			}).catch((err)=>{
				res.status(500).send(err);
			});
		});
		
		app.put(`/${Rest.options.base}/:id`, (req, res)=>{
			res.send("ok");
		});
	},
	init(options){
		let _options = Object.assign({}, defaultOptions, options);
		Rest.options = _options;
		Rest.service = _options.service;
		Rest.initRouters(_options.routerRef);
	}
}

module.exports = Rest;