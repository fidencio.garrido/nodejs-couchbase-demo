const couchbase = require("couchbase"),
	N1qlQuery = couchbase.N1qlQuery,
	uuid = require("uuid"),
	log = require("winston"),
	SERVER = process.env.DBHOST || "localhost",
	BUCKET = process.env.BUCKET || "default",
	Readable = require("stream").Readable;


class DB {
	
	constructor(){
		console.log(`DBHOST=${SERVER} BUCKET=${BUCKET}`);
		this.cluster = new couchbase.Cluster(`couchbase://${SERVER}`);
		this.bucket = this.cluster.openBucket(BUCKET);
		console.log("connected");
	}
	
	findAll(){
		const self = this;
		return new Promise((resolve, reject)=>{
			let q = N1qlQuery.fromString("select * from default");
			self.bucket.query(q, (err, rows, meta)=>{
				if (err){
					console.dir(err);
					reject(err);
				}
				resolve(rows);
			});
		});
	}
	
	findAllStream(stream){
		const q = N1qlQuery.fromString("select * from default"),
			req=this.bucket.query(q),
			rs = new Readable({
				read(size){
					
				}
			});
		
		req.on("error", (err)=>{
			stream.status(500).send(err);
		});
		
		req.on("row", (r)=>{
			rs.push(JSON.stringify(r));
		});
		
		req.on("end", (meta)=>{
			rs.push(null);
		});
		
		rs.pipe(stream);
	}
	
	create(item){
		const self = this;
		item.id = item.id || uuid.v4();
		return new Promise( (resolve, reject)=>{
			self.bucket.insert(item.id, item, {}, (err, res)=>{
				if (err){
					log.error(err);
					reject(err);
				} else{
					resolve(item);
				}
			});
		});
	}
}

module.exports = DB;