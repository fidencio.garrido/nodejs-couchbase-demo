# NodeJS Demo

This project illustrates:

* A REST Service that receives data about movies through its API and, 
returns the list of titles in a traditional way (fetching and returning) or
streaming.
* A set of clients that send the metadata to be stored using different 
approaches.

The objective is to illustrate the use in streams in client applications to 
read and throttle data and the use of streams in server applications to improve
the clients (as in mobile or web clients) experience while reading data.

## Requirements

* NodeJS 6.x
* Couchbase
* (Optional) Docker and docker compose

# Service

Http Interface, to start it:

```sh
DBHOST=<couchbasehostname_or_ip> BUCKET=<bucketname> node index
```

Create a docker container
```sh
./docker.sh
```

## Interface

### Populating metadata

```POST /metadata```

The payload is format free, any object can be passed. 


### Fetching all the catalog

```GET /metadata```

This API uses the traditional approach where the server queries the database
and once all the results are retrieved, the whole set is sent to the (mobile/web)
client. Therefore, client services should expect a delay (~20 seconds) before
starting to receive the data provided in the example on top of the data transfer
time.


### Fetching all the catalog using a stream

```GET /metadata?stream=true```

This API uses a data stream (available in Couchbase). The API will start sending
individuals rows in a continous stream, one row at the time if the buffer allows
it. Client experience is improved, results start flowing within the first 
second, thus, the transfer time will just depend in the amount of data.

Since individual results are pushed one by one, the stream does not include the 
```[]``` at the begining and end respectively indicating the response is an 
array. The workaround is to push ```[``` before the data and ```]``` on the 
```on("end", ()=>{});``` event in the DB call.

The memory usage on the streaming option is optimized.

# Client

There are three different flavors of the client used to illustrate different
features:

* ```client.js```. It will just break due to a bad design. Uses blocking I/O.
* ```clientv2.js```. It will succeed but the actual data transfer won't start
  until the whole source file is read.
* ```clientv3.js```. Implements data streaming allowing to create records
  within the first 5 seconds. It is also the most optimized version for
  memory resources.

```sh
SERVER=<ServiceIP:Port> node client<v2|v3>.js
```
