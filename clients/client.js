const fs = require("fs"),
	request = require("superagent"),
	faker = require("faker"),
	SERVER = process.env.SERVER || "localhost:3000";

console.log(`SERVER=${SERVER}`);

let saved = 0,
	errors = 0;


console.time("process");

function sendMovie(movie){
	if(movie && typeof movie.title!=="undefined"){
		request.post(`http://${SERVER}/metadata`).send(movie).end((error, res)=>{
			if (error){
				errors++;
			} else{
				saved++;
			}
			console.log(`sucess=${saved} errors=${errors}`);
		});
	}
}

function string2movie(line){
	const sections = line.split(","),
			id = sections[0],
			title = sections[1],
			genre = (sections[2] === "(no genres listed)" || typeof sections[2]==="undefined") ? [] : sections[2].split("|"),
			actors = [];

		if (genre.length>0){
			genre[genre.length-1] = genre[genre.length-1].replace("\r", "");
		}
		
		let lorem ="";
		for(let i=0; i<15;i++){
			let n = faker.fake("{{name.lastName}}, {{name.firstName}} {{name.suffix}}");
			lorem+=faker.lorem.sentence();
			actors.push(n);
		}
		
		const movie = {id: id, title: title, genre: genre, actors: actors, price: faker.fake("commerce.price"), company: faker.fake("company.companyName"), more: lorem};
		sendMovie(movie);
}

fs.readFile("movies.csv", "utf-8", (err,content)=>{
	if (err){
		throw "Cannot open file";
	}
	const lines = content.split("\n");
	for (let line of lines){
		string2movie(line);
	}
});