const fs = require("fs"),
	request = require("superagent"),
	faker = require("faker"),
	SERVER = process.env.SERVER || "localhost:3000",
	queue = [];
	
let rate = 100;

console.log(`SERVER=${SERVER}`);

let buffer = "";

let saved = 0,
	errors = 0,
	running = 0,
	processed = false;

console.time("process");

let interval = setInterval(()=>{
	if (queue.length === 0 && processed && running === 0){
		console.timeEnd("process");
		process.exit(0);
	}
	console.log(`q=${queue.length} running=${running} rate=${rate}`);
	if (queue.length>0 && (running < rate)){
		let dequeuer = rate - running;
		for(let i=0; i<dequeuer; i++){
			let m = queue.pop();
			if ( m!== null){
				sendMovie(m);
			}
		}
	}
}, 200);

function queueMovie(movie){
	if(movie && typeof movie.title!=="undefined"){
		queue.push(movie);
	}
}

function sendMovie(movie){
	if(movie && typeof movie.title!=="undefined"){
		running++;
		request.post(`http://${SERVER}/metadata`).send(movie).end((error, res)=>{
			running--;
			if (error){
				errors++;
			} else{
				saved++;
			}
			console.log(`sucess=${saved} errors=${errors}`);
		});
	}
}

function string2movie(line){
	const sections = line.split(","),
			id = sections[0],
			title = sections[1],
			genre = (sections[2] === "(no genres listed)" || typeof sections[2]==="undefined") ? [] : sections[2].split("|"),
			actors = [];

		if (genre.length>0){
			genre[genre.length-1] = genre[genre.length-1].replace("\r", "");
		}
		
		let lorem ="";
		for(let i=0; i<15;i++){
			let n = faker.fake("{{name.lastName}}, {{name.firstName}} {{name.suffix}}");
			lorem+=faker.lorem.sentence();
			actors.push(n);
		}
		
		const movie = {id: id, title: title, genre: genre, actors: actors, price: faker.fake("commerce.price"), company: faker.fake("company.companyName"), more: lorem};
		queueMovie(movie);
}

const stream = fs.createReadStream("./movies.csv");

stream.on("data", (d)=>{
	const s = (buffer + d.toString()).split("\r");
	buffer = "";
	if (s.length>0){
		for(let i=0; i<s.length; i++){
			if ( i !== s.length-1){ 
				const movie = s[i].replace("\r", "").replace("\n","");
				string2movie(movie);
			} else{
				buffer+=s[i];
			}
		}
	}
});

stream.on("end", ()=>{
	console.log("Reading file completed");
	processed = true;
	setTimeout(()=>{
		rate = 160;
	}, 5000);
	console.log(queue.length);
});